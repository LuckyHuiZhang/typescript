"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Shape = void 0;
//superclass 
class Shape {
    //constructor with parameter properties
    constructor(_x, _y) {
        this._x = _x;
        this._y = _y;
    }
    //getter and setter
    get x() {
        return this._x;
    }
    set x(value) {
        this._x = value;
    }
    get y() {
        return this._y;
    }
    set y(value) {
        this._y = value;
    }
    //method 
    getInfo() {
        return `x=${this._x}, y=${this._y}`;
    }
}
exports.Shape = Shape;
