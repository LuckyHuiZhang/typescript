//superclass 
export class Shape {
  //constructor with parameter properties
  constructor(private _x: number, private _y: number) {}

    //getter and setter
  public get x(): number {
    return this._x;
  }
  public set x(value: number) {
    this._x = value;
  }

  public get y(): number {
    return this._y;
  }
  public set y(value: number) {
    this._y = value;
  }

    //method 
  getInfo(): string {
    return `x=${this._x}, y=${this._y}`;
  }
}